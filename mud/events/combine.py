# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3

class CombineOnEvent(Event2):
    NAME = "combine-on"

    def perform(self):
        if not self.object.has_prop("combinable"):
            self.fail()
            return self.inform("combine-on.failed")
        self.inform("combine-on")


class CombineOffEvent(Event2):
    NAME = "combine-off"

    def perform(self):
        if not self.object.has_prop("combinable"):
            self.fail()
            return self.inform("combine-off.failed")
        self.inform("combine-off")

class CombineWithEvent(Event3):
    NAME = "combine-on-with"

    def perform(self):
        if not self.object.has_prop("combinable"):
            self.fail()
            return self.inform("combine-on-with.failed")
        if not self.object2.has_prop("combinable"):
            self.fail()
            return self.inform("combine-on-with.failed")
        self.inform("combine-on-with")
        print("allo")
